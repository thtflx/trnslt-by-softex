import React from 'react'
import { translate } from '@vitalets/google-translate-api';


const App = () => {

  const text = translate('Привет, мир! Как дела?', { to: 'en' });
  console.log('TEXT', text)

  // Вызов функции


  return (
    <div style={{ display: 'flex', justifyContent: 'center', marginTop: 'auto', marginBottom: 'auto', fontFamily: 'monospace' }}>
      <div>
        <h1 style={{ fontSize: '30px', display: 'flex', justifyContent: 'center', marginBottom: '30px' }}>TRNSLT</h1>

        <div style={{ display: 'flex', alignItems: 'center', gap: '200px' }}>
          <div>
            <h3>So'zlarni kiriting:</h3>
            <input type="text" style={{ border: '3px solid black', borderRadius: '5px' }} />
            <div style={{ marginTop: '10px' }}>
              <button style={{ border: '2px solid black', padding: ' 2px 15px  2px 15px', }}>Click</button>
            </div>
          </div>

          <div style={{ display: 'flex', alignItems: 'center', gap: '20px' }}>
            <div className='wrapRes'>
              <div>
                <h3>uz</h3>
                <p style={{ border: '3px solid black', borderRadius: '5px', padding: '30px 10px', width: '250px' }}></p>
              </div>
              <div>
                <h3>ru</h3>
                <p style={{ border: '3px solid black', borderRadius: '5px', padding: '30px 10px', width: '250px' }}></p>
              </div>
            </div>

            <div className='wrapRes'>
              <div>
                <h3>eng</h3>
                <p style={{ border: '3px solid black', borderRadius: '5px', padding: '30px 10px', width: '250px' }}></p>
              </div>
              <div>
                <h3>tr</h3>
                <p style={{ border: '3px solid black', borderRadius: '5px', padding: '30px 10px', width: '250px' }}></p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default App






